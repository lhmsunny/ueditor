# ueditor

#### 介绍
ueditor java改造版本，兼容jar模式打包运行

#### 软件架构
软件架构说明 java版本


#### 使用说明

1.下载项目并打包jar ，本地仓库有后

2.  在pom引入

```
        <!-- ueditor 定制版  https://gitee.com/lhmsunny/ueditor  -->
        <dependency>
            <groupId>com.baidu</groupId>
            <artifactId>ueditor</artifactId>
            <version>1.4.3</version>
        </dependency>
```

2.  主要修改文件为 src/main/resources/config.json    和webapp/theme/ueditor/ueditor.config.js  针对域名的访问地址进行调整
3.  java使用为(此处展示代码为jfinal),可根据代码自行调整(需添加跨域拦截和uploadPath配置)

```
@Path("/ueditor")
public class UeditorController extends Controller {

	// 推送核心API接口
	public void index() {
		String result = new ActionEnter(PropKit.get("uploadPath"),getRequest()).exec();
		renderText(result);
	}
}

```
#附件路径windows
uploadPath=D:/lhm/upload
#附件路径linux
uploadPath=/lhm/upload



#### 参与贡献

1.  百度Ueditor 1.4.3

#### 建议
定制化调整较大，建议下载源码参考调整，修复了很多BUG，并兼容jar包模式运行 ，本项目在另一开源项目下oss有详细应用
https://gitee.com/lhmsunny/jfinal-projects/oss

